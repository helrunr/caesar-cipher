#include <stdio.h>
#include <stdlib.h>

char data[50], tmp;
int key, counter;

void prompt()
{
	printf("Enter String to Encrypt: ");
	
	scanf("%[^\n]s", data);
}

void shift_val()
{ 
	printf("Enter a Shift Value: ");
	
	scanf("%d", &key);
}

void encrypt() {
	for (counter = 0; data[counter] != '\0'; counter++)
	{	
		tmp = data[counter];

		if ((tmp + key) > 0x7E)
		{
			tmp = 0x21 + (tmp + key - 0x7E) - 1;
		}
		else if ((tmp + key) < 0x7E)
		{
			tmp = tmp + key;	
		}
		else if ((tmp + key) == 0x7E)
		{
			tmp = 0x7E;
		}

		data[counter] = tmp;
	}		

	printf("\nEncrypted String: %s\n", data);
}

int main()
{	
	int u_val;
	prompt();
	shift_val();
	encrypt();
	return 0;
}
