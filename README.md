# caesar cipher

Caesar cipher using ASCII characters from 0x33 to 0x7E. User provides string and shift value.
Decryption of string when key/shift value is known. 

Originally written and compiled on Debian Linux using gcc.
