#include <stdio.h>
#include <stdlib.h>

char data[50], tmp;
int key, counter;

void prompt()
{
	printf("Enter string to decrypt: ");

	scanf("%[^\n]s", data);
}

void shift_val()
{
	printf("Enter the Shift Value: ");

	scanf("%d", &key);
}

void decrypt()
{
	for (counter = 0; data[counter] != '\0'; counter++)
	{
		tmp = data[counter];
		
		if ((tmp - key) < 0x21)
		{
			tmp = 0x7E - ((0x21 - tmp) + key ) + 1;
		}
		else if ((tmp - key) > 0x21)
		{
			tmp = tmp - key;
		}

		data[counter] = tmp;
	}
	
	printf("\nDecrypted String: %s\n", data);
}

int main()
{
	prompt();
	shift_val();
	decrypt();

	return 0;
}
